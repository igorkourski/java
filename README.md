# java

project <one-parent> has two maven-modules:

one-lib:  some api-related & utilities code

one-mod:  seven java projects:		

- calculator, model of process console based on swing; 
- threadpool, framework to manage multi-threding processing;
- threadmonitor, monitoring multi-threading processing;

- codility/codility_dec11 - solutions for the tasks from codility.com
- timer, model to run a proccess by timer 
- imageExtractor, extractor images from the web-library
- threadman, moving imageExtractor to multi-threading framework