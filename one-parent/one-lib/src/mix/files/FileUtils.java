package files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileUtls {
	
	// shows how to create multiple directories in java
	// (multiple directory levels)
	public static boolean createMultipleDir( String name){

	    // the folders "000/111/222" don't exist initially
	    File dir = new File(name);
	    
	    // create multiple directories at one time
	    return dir.mkdirs();
	}
	
	//Here�s the source code for this �list files in a directory� Java class:
	public static List<String> scanDir (String name) throws IOException{
		    // create a file that is really a directory
		    File aDirectory = new File(name);

		    // get a listing of all files in the directory
		    String[] filesInDir = aDirectory.list();

		    // sort the list of files (optional)
		    // Arrays.sort(filesInDir);
		    
		    
	    	String dirName = name;
	    	if (dirName.endsWith("/")) {
	    		
	    	} else {
	    		dirName = dirName + "/";
	    	}
	    	
	    	String dirName2 = aDirectory.getPath();
	    	//String dirName3 = aDirectory.getAbsolutePath();
	    	//String dirName4 = aDirectory.getCanonicalPath();
    	
	    	if (dirName2.endsWith("\\")) {
	    		
	    	} else {
	    		dirName2 = dirName2 + "\\";
	    	}
	    	
	    	
	    	
	    	List<String> list = new ArrayList<String>();
		    for ( int i=0; i<filesInDir.length; i++ ) {
		    	
		    	String fileName = dirName2 + filesInDir[i];
		    			    	
		    	File tmp = new File(fileName);
		    	
		    	if (tmp.isDirectory()) {
		    		
			    	String fileName2 = dirName + filesInDir[i];
		    		
		    		list.addAll( scanDir( fileName2 ) );
		    	}
		    	
		    	if (tmp.isFile()) {
		    		
		    		renameFile(tmp);
		    	
			    	try {
						//URL url = new URL(tmp.getPath());
						list.add(tmp.getPath());
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e.getMessage());
					}
		    	}
		    	
		    }

		    
		    return list;
		  
	}
	
	public static String getCurrentPath() {
	    File current = new File(".");
	    return current.getAbsolutePath();
	
	}
	
	//check regexp https://regex101.com/r/b9Xkab/1
	public static long[] getNumbers(String string) {
		

		final String regex = "(?<=)\\d+(?=)"; //".*\\d+.*"; //numbers

		//final String regex = "(?<=\\()\\d+(?=\\))"; 
		//number inside brackets 
		//final String string = "\"hadoop (1899)\"  \"hadoop(yarn)(1980)\"";

		final Pattern pattern = Pattern.compile(regex);
		final Matcher matcher = pattern.matcher(string);

		long[] array = new long[100];
		int ind = 0;
		while (matcher.find()) {
			
			String per = matcher.group(0);
			try {
				array[ind] = Long.parseLong(per);
				ind++;
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

								
			//array = new int[matcher.groupCount()];
		    //for (int i = 0; i < matcher.groupCount(); i++) {
		    //   array[i] = Integer.parseInt(matcher.group(i));
		    //}

			
		    //System.out.println("Full match: " + matcher.group(0));
		    //for (int i = 1; i <= matcher.groupCount(); i++) {
		     //   System.out.println("Group " + i + ": " + matcher.group(i));
		    //}
		}
		long[] correct = new long[ind];
		for(int i=0;i<ind;i++) {
			correct[i] = array[i];
		}
		
		return correct;
		
	}
	
	public static File renameFile(File file) {
		
		if (file!=null && file.isFile()) {
			
			//new name
			String old = file.getName();
			String dir = file.getParent();
			String newName = null;
			
			//Path path = Paths.get(file.getPath());
			//path.get
			
			
			if(old.contains("_")) {
				//String[] parts = old.split("_");
				//int num1 = Integer.parseInt(parts[0]);
				
			}
			
			//while (file.renameTo(new File(dir, "" + num + ".pdf"))) {
				
			//}
			

		}
		
		
		return new File(".");
	}
	
	static List<String> blackList = new ArrayList<String>();
	static {
		blackList.add("one");
	}
	
	public static boolean isBlackListed(String original) {
		
		for(String black : blackList) {
			if (original.contains(black)) {
				return true;
			}
		}
		return false;
		
	}
	
	public static boolean replaceSiteRefs( String path, String siteFrom, String siteTo) {
		
	   try {
		        // input the file content to the StringBuffer "input"
		        BufferedReader file = new BufferedReader(new FileReader(path));
		        String line;
		        StringBuffer inputBuffer = new StringBuffer();

		        while ((line = file.readLine()) != null) {
		            inputBuffer.append(line);
		            inputBuffer.append('\n');
		        }
		        String inputStr = inputBuffer.toString();

		        file.close();
		        System.out.println(inputStr); // check that it's inputted right

		        //
				Pattern ptrn = Pattern.compile(siteFrom);
				Matcher matcher = ptrn.matcher(inputStr);
				while(matcher.find()){
				  
					//if not black listed text
					String change = inputStr.substring(matcher.end(), matcher.end() + 100);
					if (isBlackListed(change)) {
						
					} else {
						
						String start = inputStr.substring(0,matcher.start());
						String end = inputStr.substring(matcher.start()+siteFrom.length(),inputStr.length());
						inputStr = start + siteTo + end;
								
					}
				}
		        // check if the new input is right
		        System.out.println("----------------------------------\n"  + inputStr);

		        //
		        // write the new String with the replaced line OVER the same file
		        //FileOutputStream fileOut = new FileOutputStream("notes.txt");
		        //fileOut.write(inputStr.getBytes());
		        //fileOut.close();

	    } catch (Exception e) {
		        System.out.println("Problem reading file.");
	    }
		
		
		
		return true;
	}
	
	/*
	 * move (copy?) whole directory (with file renaming/) 
	 */
	
	public static void moveDirectory(String dir) {
		List<String> filesInDir = null;
		try {
			filesInDir = scanDir(dir);
			Collections.sort( filesInDir );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	    // have everything i need, just print it now
	    if (filesInDir!=null) {
			for ( int i=0; i<filesInDir.size(); i++ ){
				
				//rename file
				String fileName = filesInDir.get(i).substring(filesInDir.get(i).lastIndexOf("\\")+1);
				long[] nums = getNumbers(fileName);
				String numsPrint = nums.length>0?Arrays.toString(nums):"";    //((nums.length > 0):nums.toString();"");
				
				System.out.println( i + ". " + filesInDir.get(i) + "  name=" + fileName + "  "  + numsPrint );
				
				//check file content 
				String fileExt = fileName.substring(fileName.lastIndexOf(".")+1,fileName.length());
				if (fileExt.equalsIgnoreCase("html") || fileExt.equalsIgnoreCase("js") || fileExt.equalsIgnoreCase("css"))
					replaceSiteRefs( filesInDir.get(i),"igorkourski.000webhostapp.com","igork.github.io");
				
		    }
	    }	
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//String dir = "C:/Users/Igor/Downloads/Ant";
		String dir = "C:/Users/Igor/Desktop/SITE_ARCHIVE";
		moveDirectory(dir);
		

	}

}
