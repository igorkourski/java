package api;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.HttpCookie;
import java.net.CookieManager;

public class ApiCall2 {
	
	public static String class2json(Object obj) {
		
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(obj);

		return null;
	}
	
	public static String json2class(String json) {
		
		//Gson gson = new GsonBuilder().create(); 
		//Response r = gson.fromJson(jsonString, Response.class);

		return null;
	}
	
	public static <T> T getClass(Class<T> param, String json){
		
		Gson gson = new GsonBuilder().create(); 
		T r = gson.fromJson(json, param);
		
		return r;
	}
	
	public static String get(URL url, Properties headers, String payload) throws Exception {
		String response = "";
		

		/*
		3. Creating a Request
		A HttpUrlConnection instance is created by using the openConnection() method of the URL class. Note that this method only creates a connection object, but does not establish the connection yet.
		The HttpUrlConnection class is used for all types of requests by setting the requestMethod attribute to one of the values: GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE.
		Let�s create a connection to a given url using GET method:
		*/
			
		//URL url = new URL("http://example.com");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		
		/*
		4. Adding Request Parameters
		If we want to add parameters to a request, we have to set the doOutput property to true, then write a String of the form param1=value&param2=value to the OutputStream of the HttpUrlConnection instance:
		*/
		boolean isParam = false;
		if (isParam ){
			Map<String, String> parameters = new HashMap<>();
			parameters.put("param1", "val");
			 
			con.setDoOutput(true);
			DataOutputStream out = new DataOutputStream(con.getOutputStream());
			out.writeBytes(Util.getParamsString(parameters));
			out.flush();
			out.close();
		}
		
		/*
		5. Setting Request Headers

		Adding headers to a request can be achieved by using the setRequestProperty() method:
		*/
			
		con.setRequestProperty("Content-Type", "application/json");

		/*
		To read the value of a header from a connection, we can use the getHeaderField() method:
		*/		
		//String contentType = con.getHeaderField("Content-Type");
		
		/*
		6. Configuring Timeouts
		HttpUrlConnection class allows setting the connect and read timeouts. These values define the interval of time to wait for the connection to the server to be established or data to be available for reading.
		To set the timeout values we can use the setConnectTimeout() and setReadTimeout() methods:
		*/
			
		con.setConnectTimeout(5000);
		con.setReadTimeout(5000);

		/*
		7. Handling Cookies
		The java.net package contains classes that ease working with cookies such as CookieManager and HttpCookie.
		First, to read the cookies from a response, we can retrieve the value of the Set-Cookie header and parse it to a list of HttpCookie objects:
		*/
		
		String cookiesHeader = con.getHeaderField("Set-Cookie");
		if (cookiesHeader!=null) {
			List<HttpCookie> cookies = HttpCookie.parse(cookiesHeader);

			/*
			Next, we will add the cookies to the cookie store:
			*/
				
			CookieManager man = new CookieManager();
			
			cookies.forEach(cookie -> man.getCookieStore().add(null, cookie));

		/*
		Let�s check if a cookie called username is present, and if not, we will add it to the cookie store with a value of �john�:
		*/
			
			Optional<HttpCookie> usernameCookie = cookies.stream()
			  .findAny().filter(cookie -> cookie.getName().equals("username"));
			if (usernameCookie == null) {
			    man.getCookieStore().add(null, new HttpCookie("username", "john"));
			}

			/*
			Finally, to add the cookies to the request, we need to set the Cookie header, after closing and reopening the connection:
			*/
				
			con.disconnect();
			try {
				con = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		 
		}
		//con.setRequestProperty("Cookie", 
		//  StringUtil.join(man.getCookieStore().getCookies(), ";"));
		
		/*
		8. Handling Redirects
		We can enable or disable automatically following redirects for a specific connection by using the setInstanceFollowRedirects() method with true or false parameter:
		*/
		con.setInstanceFollowRedirects(false);

		/*
		It is also possible to enable or disable automatic redirect for all connections:
		*/
		
		HttpURLConnection.setFollowRedirects(false);

	
		/*
		9. Reading the Response
		Reading the response of the request can be done by parsing the InputStream of the HttpUrlConnection instance.
		To execute the request we can use the getResponseCode(), connect(), getInputStream() or getOutputStream() methods:
		*/
			
		int status = con.getResponseCode();

		
		/*
		By default, the behavior is enabled.
		When a request returns a status code 301 or 302, indicating a redirect, we can retrieve the Location header and create a new request to the new URL:
		*/
		
			
		if (status == HttpURLConnection.HTTP_MOVED_TEMP
		  || status == HttpURLConnection.HTTP_MOVED_PERM) {
		    String location = con.getHeaderField("Location");
		    URL newUrl = new URL(location);
		    con = (HttpURLConnection) newUrl.openConnection();
		}
		
		
		/*
		Finally, let�s read the response of the request and place it in a content String:
		*/
		
		BufferedReader in = new BufferedReader(
		  new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
		    content.append(inputLine);
		}
		in.close();

		/*
		To close the connection, we can use the disconnect() method:
		*/
		StringBuilder builder = new StringBuilder();
		builder.append("Response Code: ")
			.append(con.getResponseCode())
			.append(" ResponseMessage: ")
			.append(con.getResponseMessage())
			.append("\n");
		
		Map<String, List<String>> map = con.getHeaderFields();
		for (Map.Entry<String, List<String>> entry : map.entrySet())
		{
		    if (entry.getKey() == null) 
		        continue;
		    builder.append( entry.getKey())
		           .append(": ");

		    List<String> headerValues = entry.getValue();
		    Iterator<String> it = headerValues.iterator();
		    if (it.hasNext()) {
		        builder.append(it.next());

		        while (it.hasNext()) {
		            builder.append(", ")
		                   .append(it.next());
		        }
		    }

		    builder.append("\n");
		}
		System.out.println(builder);
		
		con.disconnect();
		
		
		return content.toString();
	}
	
	public static void main(String[] args) throws Exception {

		/*
		3. Creating a Request
		A HttpUrlConnection instance is created by using the openConnection() method of the URL class. Note that this method only creates a connection object, but does not establish the connection yet.
		The HttpUrlConnection class is used for all types of requests by setting the requestMethod attribute to one of the values: GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE.
		Let�s create a connection to a given url using GET method:
		*/
			
		URL url = new URL("http://example.com");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		
		/*
		4. Adding Request Parameters
		If we want to add parameters to a request, we have to set the doOutput property to true, then write a String of the form param1=value&param2=value to the OutputStream of the HttpUrlConnection instance:
		*/
		Map<String, String> parameters = new HashMap<>();
		parameters.put("param1", "val");
		 
		con.setDoOutput(true);
		DataOutputStream out = new DataOutputStream(con.getOutputStream());
		out.writeBytes(Util.getParamsString(parameters));
		out.flush();
		out.close();
		
		/*
		5. Setting Request Headers

		Adding headers to a request can be achieved by using the setRequestProperty() method:
		*/
			
		con.setRequestProperty("Content-Type", "application/json");

		/*
		To read the value of a header from a connection, we can use the getHeaderField() method:
		*/		
		String contentType = con.getHeaderField("Content-Type");
		
		/*
		6. Configuring Timeouts
		HttpUrlConnection class allows setting the connect and read timeouts. These values define the interval of time to wait for the connection to the server to be established or data to be available for reading.
		To set the timeout values we can use the setConnectTimeout() and setReadTimeout() methods:
		*/
			
		con.setConnectTimeout(5000);
		con.setReadTimeout(5000);

		/*
		7. Handling Cookies
		The java.net package contains classes that ease working with cookies such as CookieManager and HttpCookie.
		First, to read the cookies from a response, we can retrieve the value of the Set-Cookie header and parse it to a list of HttpCookie objects:
		*/
		
		String cookiesHeader = con.getHeaderField("Set-Cookie");
		List<HttpCookie> cookies = HttpCookie.parse(cookiesHeader);

		/*
		Next, we will add the cookies to the cookie store:
		*/
			
		CookieManager man = new CookieManager();
		
		cookies.forEach(cookie -> man.getCookieStore().add(null, cookie));

		/*
		Let�s check if a cookie called username is present, and if not, we will add it to the cookie store with a value of �john�:
		*/
			
		Optional<HttpCookie> usernameCookie = cookies.stream()
		  .findAny().filter(cookie -> cookie.getName().equals("username"));
		if (usernameCookie == null) {
		    man.getCookieStore().add(null, new HttpCookie("username", "john"));
		}

		/*
		Finally, to add the cookies to the request, we need to set the Cookie header, after closing and reopening the connection:
		*/
			
		con.disconnect();
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		//con.setRequestProperty("Cookie", 
		//  StringUtils.join(man.getCookieStore().getCookies(), ";"));
		
		/*
		8. Handling Redirects
		We can enable or disable automatically following redirects for a specific connection by using the setInstanceFollowRedirects() method with true or false parameter:
		*/
		con.setInstanceFollowRedirects(false);

		/*
		It is also possible to enable or disable automatic redirect for all connections:
		*/
		
		HttpURLConnection.setFollowRedirects(false);

	
		/*
		9. Reading the Response
		Reading the response of the request can be done by parsing the InputStream of the HttpUrlConnection instance.
		To execute the request we can use the getResponseCode(), connect(), getInputStream() or getOutputStream() methods:
		*/
			
		int status = con.getResponseCode();

		
		/*
		By default, the behavior is enabled.
		When a request returns a status code 301 or 302, indicating a redirect, we can retrieve the Location header and create a new request to the new URL:
		*/
		
			
		if (status == HttpURLConnection.HTTP_MOVED_TEMP
		  || status == HttpURLConnection.HTTP_MOVED_PERM) {
		    String location = con.getHeaderField("Location");
		    URL newUrl = new URL(location);
		    con = (HttpURLConnection) newUrl.openConnection();
		}
		
		
		/*
		Finally, let�s read the response of the request and place it in a content String:
		*/
		
		BufferedReader in = new BufferedReader(
		  new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
		    content.append(inputLine);
		}
		in.close();

		/*
		To close the connection, we can use the disconnect() method:
		*/
			
		con.disconnect();
	  }
}
