package urls;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SiteTemplate {
	
	public final String defaultName = "title";
	
	public String parseForTitle(String source) {
		return null;
	}
	
	public SiteTemplate(SiteEnum site, String source) {
		this.site = site;
		this.source = source;
	}

	public List<URL> parseForURLs( String source){
		return null;
	}
	
	protected SiteEnum site;
	
	protected String source;
	
	public SiteEnum getSite() {
		return site;
	}

	public void setSite(SiteEnum site) {
		this.site = site;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	//public SiteTemplate(SiteEnum site) {
	//	this.site = site;
	//}
	
	//site distinguisher 
	/*
	final static SiteEnum[] list = {
			
			new SiteEnum(
					"/photos/gallery/",
					"<meta name=\"twitter:title\" property=\"og:title\" content=\"",
					"\"",
					"imageURL\":\"",
					"\""),
			
			new SiteEnum(
					"2read.com/",
					"<span itemprop=\"name\">",
					"</scan>",
					"",
					""),
			
			new SiteEnum(
					"comics.com/book",
					"title Book id: #",
					".</title>",
					"",
					"")
			
	};
	
	public static SiteEnum getInstance(List<SiteEnum> list, String source) {
		
		if (source!=null) {
			
			for(SiteEnum site:list) {
			
				if ( source.contains(site.getKey()) ) {
					return site;
				}
			}
			
		}
		
		return null;
	}
	
	public static SiteTemplate getSite(List<SiteEnum> list,String source) {
		
		SiteEnum site = getInstance(list,source);
		
		if (site==null) {
			
			return null;
			
		} else {
			
			if (site.getKey().equalsIgnoreCase(list[0].getKey())){
			
				return new SiteFirst(site,source);
			}
			
			if (site.getKey().equalsIgnoreCase(list[1].getKey())){
				
				return new SiteSecond(site,source);
			}
			
			return null;
			
		}
	}
	*/
	//template functions
	public static List<String> parseString2(String str, String anchor, String anchorEnd){
		
		List<String> list = new ArrayList<String>();
		
		Pattern ptrn = Pattern.compile(anchor);
		Matcher matcher = ptrn.matcher(str);
		
		while(matcher.find()){
		  
			String s2 = str.substring(matcher.end(), matcher.end()+1000);
			//int ind = s2.indexOf("}");
	
			int img = s2.indexOf(anchorEnd);
			//String s3 = str.substring(matcher.end(),matcher.end()+ind);
		
			String s4 = str.substring(matcher.end(),matcher.end()+img);
			
			s4 = s4.replaceAll("/","");
			
			try {
				
				//StringBuffer buf = new StringBuffer(s4);
				//for(int i=0; i<buf.length(); i++) {
				//	char cc = buf.charAt(i);			
				//	System.out.println(" char " + cc + " at " + i );
				//}
				
				
				
				  //Pattern removeHTMLTagsPattern = Pattern.compile("/s"); 
				  //matcher = removeHTMLTagsPattern.matcher(s4);
				  //s4 = matcher.replaceAll("\");
				
				s4 = s4.replaceAll("\\\\", "/");
				System.out.println("Found ptrn index "+ matcher.start() +" - "+ (matcher.end()-1) + "  " + s4);
				
				
				//list.add(new URL(s4));
				list.add(s4);
	
				
			} catch (Exception e) {
				e.printStackTrace();
			}
	
		}
		return list;
	}

	public static String downloadURL(String theURL){
		URL u;
		InputStream is = null;
		String s;
		StringBuffer sb = new StringBuffer();
		
		try{
		    u = new URL(theURL);
		    is = u.openStream();
		    
		    //depricated. DataInputStream dis = new DataInputStream(new BufferedInputStream(is));
		    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		    while ((s = reader.readLine()) != null){
		    	sb.append(s + "\n");
		    }
	    	is.close();
		} catch (Exception e){
		    e.printStackTrace();
		} finally {
		}
		return sb.toString();
	}
	


}
