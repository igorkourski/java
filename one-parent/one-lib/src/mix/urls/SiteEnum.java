package urls;

public class SiteEnum {
	
	private String key;
	
	private String titleAnchor;
	
	private String titleAnchorEnd;
	
	private String urlAnchor;
	
	private String urlAnchorEnd;

	public SiteEnum (String key, String title, String titleAnchorEnd, String url, String urlEnd) {
		
		this.key = key;
		
		setTitleAnchor(title);
		setTitleAnchorEnd(titleAnchorEnd);
		
		setUrlAnchor(url);
		setUrlAnchorEnd(urlEnd);
	}
	


	public String getUrlAnchor() {
		return urlAnchor;
	}

	public void setUrlAnchor(String urlAnchor) {
		this.urlAnchor = urlAnchor;
	}

	public String getTitleAnchor() {
		return titleAnchor;
	}

	public void setTitleAnchor(String titleAnchor) {
		this.titleAnchor = titleAnchor;
	}

	public String getTitleAnchorEnd() {
		return titleAnchorEnd;
	}

	public void setTitleAnchorEnd(String titleAnchorEnd) {
		this.titleAnchorEnd = titleAnchorEnd;
	}

	public String getUrlAnchorEnd() {
		return urlAnchorEnd;
	}

	public void setUrlAnchorEnd(String urlAnchorEnd) {
		this.urlAnchorEnd = urlAnchorEnd;
	}
	
	public final String getKey() {
		return key;
	}


	@Override
	public String toString() {
		return "SiteEnum [key=" + key + ", titleAnchor=" + titleAnchor + ", titleAnchorEnd=" + titleAnchorEnd
				+ ", urlAnchor=" + urlAnchor + ", urlAnchorEnd=" + urlAnchorEnd + "]";
	}

}
