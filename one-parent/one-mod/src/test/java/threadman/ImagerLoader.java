package threadman;

import threadpool.TaskPool;

public class ImagerLoader extends TaskPool {

	public static void main(String[] args) {
		
		TaskPool task = new TaskPool();
		//ImageExtract exec = new ImageExtract();
		
		task.setExecSize(5);
		task.setPoolSize(50);
		
		task.setExec(ImageExtract.class);
		
		task.run();

	}

}
