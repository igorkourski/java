package timer;


import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class timerTest extends TimerTask {

    @Override
    public void run() {
        System.out.println("Timer task started at:"+new Date());
        completeTask();
        System.out.println("Timer task finished at:"+new Date());
    }

    private void completeTask() {
        try {
            //assuming it takes 20 secs to complete the task
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /*
    Notice that one thread execution will take 20 seconds but Java Timer object is scheduled to run the task every 10 seconds. Here is the output of the program:

    	TimerTask started
    	Timer task started at:Wed Dec 26 19:16:39 PST 2012
    	Timer task finished at:Wed Dec 26 19:16:59 PST 2012
    	Timer task started at:Wed Dec 26 19:16:59 PST 2012
    	Timer task finished at:Wed Dec 26 19:17:19 PST 2012
    	Timer task started at:Wed Dec 26 19:17:19 PST 2012
    	Timer task finished at:Wed Dec 26 19:17:39 PST 2012
    	Timer task started at:Wed Dec 26 19:17:39 PST 2012
    	Timer task finished at:Wed Dec 26 19:17:59 PST 2012
    	Timer task started at:Wed Dec 26 19:17:59 PST 2012
    	Timer task finished at:Wed Dec 26 19:18:19 PST 2012
    	Timer task started at:Wed Dec 26 19:18:19 PST 2012
    	TimerTask cancelled
    	Timer task finished at:Wed Dec 26 19:18:39 PST 2012
    	The output confirms that if a task is already executing, Timer will wait for it to finish and once finished, it will start again the next task from the queue.
    */
    
    public static void main(String args[]){
        TimerTask timerTask = new timerTest();
        //running timer task as daemon thread
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(timerTask, 0, 10*1000);
        System.out.println("TimerTask started");
        //cancel after sometime
        try {
            Thread.sleep(120000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        timer.cancel();
        System.out.println("TimerTask cancelled");
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

