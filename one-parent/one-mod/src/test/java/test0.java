
class MultithreadingDemo extends Thread implements Runnable 
{ 
    public void run() {
        try
        { 
            // Displaying the thread that is running 
            System.out.println ("ThreadR " + 
                                Thread.currentThread().getId() + 
                                " is running"); 
  
        } 
        catch (Exception e) 
        { 
            // Throwing an exception 
            System.out.println ("Exception is caught"); 
        }     	
    }
}
 


// Java code for thread creation by implementing 
// the Runnable Interface 
class MultithreadingDemo1 extends MultithreadingDemo //implements Runnable 
{ 
    public void run() 
    { 
        try
        { 
            // Displaying the thread that is running 
            System.out.println ("ThreadR " + 
                                Thread.currentThread().getId() + 
                                " is running"); 
  
        } 
        catch (Exception e) 
        { 
            // Throwing an exception 
            System.out.println ("Exception is caught"); 
        } 
    } 
} 
  
//Java code for thread creation by extending 
//the Thread class 
class MultithreadingDemo2 extends MultithreadingDemo //Thread 
{ 
 public void run() 
 { 
     try
     { 
         // Displaying the thread that is running 
         System.out.println ("ThreadT " + 
               Thread.currentThread().getId() + 
               " is running"); 

     } 
     catch (Exception e) 
     { 
         // Throwing an exception 
         System.out.println ("Exception is caught"); 
     } 
 } 
} 

// Main Class 
class test0 {
	
	public static MultithreadingDemo get() {
		return (int)(Math.random()*2)==0 ? new MultithreadingDemo1() : new MultithreadingDemo2();
	}
	
    public static void main(String[] args) 
    { 
        int n = 8; // Number of threads 
        /*
        for (int i=0; i<8; i++) 
        { 
            Thread object = new Thread(new MultithreadingDemo1()); 
            object.start(); 
            
            Thread object2 = new Thread(new MultithreadingDemo2()); 
            object2.start(); 
        } 
        */
        for (int i=0; i<8; i++) {
        	
        }

    } 
} 


