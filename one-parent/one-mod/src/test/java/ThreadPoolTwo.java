// Java program to illustrate  
// ThreadPool 
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import threadmonitor.ThreadMonitor;



  
public class ThreadPoolTwo { 
     // Maximum number of threads in thread pool 
    static final int MAX_POOL = 5;    
    static final int MAX_TASKS = 50;
    
    public static void main(String[] args) 
    { 
        // creates a thread pool with MAX_POOL no. of  
        // threads as the fixed pool size(Step 1) 
        ExecutorService pool = Executors.newFixedThreadPool(MAX_POOL);   
        
        List<Future<?>> futures = new ArrayList<Future<?>>();
    	
    	// creates tasks 
    	for (int i=0; i<MAX_TASKS; i++) {
   	       	//create tast (Step 2)
    		Runnable r = new TaskTwo("task " + i); 
   	       
    		// passes the Task objects to the pool to execute (Step 3) 
    		Future f = pool.submit(r);
    		futures.add(f);
    		
    	}
        
    	//instead of shutdown (i.e. not accepting new tasks
        //pool.shutdown(); 
    	//Use A) or B)
    	// A) Await all runnables to be done (blocking)
    	//for(Future<?> future : futures)
    	//    future.get(); // get will block until the future is done

    	// B) Check if all runnables are done (non-blocking)
    	//boolean allDone = true;
    	//for(Future<?> future : futures){
    	//    allDone &= future.isDone(); // check if future is done
    	//}
    	
       	//A) Await all runnables to be done (blocking)
    	for(Future<?> future : futures)
			try {
				future.get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // get will block until the future is done

    	
    	ThreadMonitor.get().report();
    }
    


} 


