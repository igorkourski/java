
// Java program to illustrate  
// ThreadPool 
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



public class ThreadPool
{ 
     // Maximum number of threads in thread pool 
    static final int MAX_POOL = 5;    
    static final int MAX_TASKS = 50;
    
    public static void main(String[] args) 
    { 
        // creates a thread pool with MAX_POOL no. of  
        // threads as the fixed pool size(Step 1) 
        ExecutorService pool = Executors.newFixedThreadPool(MAX_POOL);   
    	
    	// creates tasks 
    	for (int i=0; i<MAX_TASKS; i++) {
   	       	//create tast (Step 2)
    		Runnable r = new Task2("task " + i); 
   	       
    		// passes the Task objects to the pool to execute (Step 3) 
    		pool.execute(r);
    	}
        
        // pool shutdown ( Step 4) 
        pool.shutdown(); 
        

    } 
} 

