import java.text.SimpleDateFormat;
import java.util.Date;

import threadmonitor.ThreadInfo;
import threadmonitor.ThreadMonitor;

class Task2 extends Thread {
	private String name; 
    
    public Task2(String s) 
    { 
        name = s; 
    }
    public void run(){    
        
    	ThreadInfo info = new ThreadInfo(this);
    	ThreadMonitor.get().active(info);
    	
     	try
        { 
     		Date d = new Date(); 
     		SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss"); 
     		System.out.println("Initialization Time for"
     				+ " task name - "+ name +" = " +ft.format(d));    
                    //prints the initialization time for every task  
                    
            Thread.sleep(1000); 
            System.out.println(name+" complete");
           	ThreadMonitor.get().completed(info);

            
        } catch(InterruptedException e) { 
            e.printStackTrace(); 
         	ThreadMonitor.get().delayed(info);
          } 
    } 
}
