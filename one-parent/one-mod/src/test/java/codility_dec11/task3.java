package codility_dec11;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/*
 * 
 */

public class task3 {
	
	public static int solution(int[] ranks) {
		int result = 0;
		
		Map<Integer,Integer> m = new HashMap<Integer,Integer>();
		
		for(int iper: ranks) {
			
			if (m.containsKey(Integer.valueOf(iper))){
				m.put(Integer.valueOf(iper), m.get(Integer.valueOf(iper))+1);
			} else {
				m.put(Integer.valueOf(iper), Integer.valueOf(1));
			}
		}
		
		
		for(Integer key: m.keySet()) {
			
			Integer reportsTo = key+1;
			Integer reportsNum = m.get(reportsTo); //key - who can report;  reportsTo-to whom
			if (reportsNum!=null) {
				result = result + m.get(key).intValue();
			}
		
		}
		return result;
    }
	
	public static void main(String[] args) {
		
		int[] rank1 = {3,4,3,0,2,2,3,0,0};
		int   iper1 = solution(rank1);
		System.out.println("1 -" + iper1);
		assert(iper1==5);
		
		int[] rank2 = {4,2,0};
		int   iper2 = solution(rank2);
		System.out.println("2 -" + iper2);
		assert(iper2==0);
		
		int[] rank3 = {4,4,3,3,1,0};
		int   iper3 = solution(rank3);
		System.out.println("3 -" + iper3);
		assert(iper1==3);
		
	}

}
