package okta_exam;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class arabicRoman {
	
	static TreeMap<Integer, String> m = new TreeMap<Integer, String>();
	/*
	static {
        m.put(100, "C");
        m.put(90, "XC");
        m.put(50, "L");
        m.put(40, "XL");
        m.put(10, "X");
        m.put(9, "IX");
        m.put(8, "VIII");
        m.put(7, "VII");
        m.put(6, "VI");
        m.put(5, "V");
        m.put(4, "IV");
        m.put(3, "III");
        m.put(2, "II");
        m.put(1, "I");

    } */
	static {
        m.put(1, "I");
        m.put(2, "II");
        m.put(100, "C");
        m.put(90, "XC");
        m.put(50, "L");
        m.put(40, "XL");
        m.put(10, "X");
        m.put(9, "IX");
        m.put(8, "VIII");
        m.put(7, "VII");
        m.put(6, "VI");
        m.put(5, "V");
        m.put(4, "IV");
        m.put(3, "III");



    }

    public static String toRoman(int number) {
        if ( number == 0 ) {
            return "";
        }
    	Integer iper =  (Integer) m.floorKey(Integer.valueOf(number));
        return m.get(iper) + toRoman(number-iper);
    }
    
    public static String toRoman2(int num) {
        StringBuilder sb = new StringBuilder();
        int times = 0;
        String[] romans = new String[] { "I", "IV", "V", "IX", "X", "XL", "L",
                "XC", "C", "CD", "D", "CM", "M" };
        int[] ints = new int[] { 1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500,
                900, 1000 };
        for (int i = ints.length - 1; i >= 0; i--) {
            times = num / ints[i];
            num %= ints[i];
            while (times > 0) {
                sb.append(romans[i]);
                times--;
            }
        }
        return sb.toString();
    } 
    
    public static String toRoman3(int num) {
        StringBuilder sb = new StringBuilder();
        int times = 0;
        String[] romans = new String[] { "I", "IV", "V", "IX", "X", "XL", "L",
                "XC", "C", "CD", "D", "CM", "M" };
        int[] ints = new int[] { 1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500,
                900, 1000 };
        

        
        for (int i = ints.length - 1; i >= 0; i--) {
            times = num / ints[i];
            num %= ints[i];
            while (times > 0) {
                sb.append(romans[i]);
                times--;
            }
        }
        return sb.toString();
    } 
	
    public static int toArabic(String test) {
    	
	    int result = 0;
	    int[] decimal = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
	    String[] roman = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
	
	    // Test string, the number 895
	    //String test = "DCCCXCV";
	
	    for (int i = 0; i < decimal.length; i++ ) {
	        while (test.indexOf(roman[i]) == 0) {
	            result += decimal[i];
	            test = test.substring(roman[i].length());
	        }
	    }
	    return result;
	    
	    
	
    }
    
    public static int toArabic2(String test) {
        
        int result = 0;
    	if ( test.isEmpty() ) {
	         return 0;
	    }
	    
	    for (Map.Entry<Integer, String> entry : m.entrySet()){

	    	if( test.indexOf(entry.getValue())==0 ) {
	    		result = entry.getKey() + toArabic2(test.substring(entry.getKey()));
	    	    return result;
	    	}
	    	
	    }
  
   //System.out.println(result);
	    return result;
    }
    
    @Test
	public void test() {

	    for (Map.Entry<Integer, String> entry : m.entrySet()){
	        System.out.println(entry.getKey() + "/" + entry.getValue());
	    }
    	
    	toArabic2("III");
    	
    	toArabic2("IV");
    		  


		for(int i=1; i<100; i++) {
			System.out.println(i + " - " + toRoman(i) + " - " + toArabic2(toRoman(i)));
			assertEquals(i,toArabic2(toRoman(i)));
		}
		
	}

}
