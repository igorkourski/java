package url2html;

//JavaUrlExample.java

import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;

public class PageExtractor {

	public static void main(String[] origin){
		
		String[] args = new String[1];
		args[0] = "http://igorkourski.000webhostapp.com/";
		
		//new JavaUrlExample(args[0]);
		for (String page:args) {
			
			if (!page.startsWith("\\$")) {
				System.out.println(page);
				new PageExtractor(page);
			} else {
				System.out.println(page);
				break;
			}
			
		}
	}
	
	public String getDestination(String tempPath, String name) {
		String dir = null;
		boolean success = false;
		String	destName = null;
		
		if (tempPath!=null) {
			
			dir = tempPath.trim();
			
			dir = dir.replaceAll("\\\\", "/");
			
			if(!dir.endsWith("/")) {
				dir = dir + "/";
			}
		
			for(int i=0; (i<50)&&!success; i++) {
				destName = tempPath + name + i;
				File dest = new File(destName);
				if (dest.exists()) {
					//
	 			} else {
	 				success = dest.mkdir();
	 			}
			}
		}
			
		return success?destName+"/":null;
	}


	public PageExtractor(String source){
	
		//String path = "C:\\Users\\Igor\\eclipse-workspace\\one\\one-parent\\one-mod\\files\\";
		//String path = "C:\\Users\\Igor\\eclipse-workspace\\one\\one-parent\\one-mod\\pages\\";
		
		//The resulting File object f represents the .jar file that was executed
		//.getURI fixes the issue with showing %20 instead of space 
		File classPath = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
		String runPath = classPath.getAbsolutePath();
		System.out.println("Running:" + runPath);
		
		String abs = (new File("src/test/resources")).getAbsolutePath();
		System.out.println("Running:" + abs);
		
		//
		if (runPath.endsWith("target\\test-classes")) {
			runPath = runPath.substring(0,runPath.lastIndexOf("target\\test-classes"));
		}
		
		String path = getDestination(runPath,"pages");						///---------------
		
		writeNote(path,source);
		
		////
		//String contents = downloadURL("https://xhamster.com/photos/gallery/super-cute-girls-having-sex-with-ugly-boys-mans-10894390/3");//"http://www.devdaily.com/");  // a sample URL
		String contents = downloadURL(source); //"https://xhamster.com/photos/gallery/sweet-katya-sucking-cock-in-the-woods-10692810");
	
		System.out.println("RAW CONTENTS:" + "\n" + contents);
	
		List<URL> strippedContents = null;
		try {
			strippedContents = parseString3(contents,source);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	    long done = 0L;
	    
		try {
			for(URL u : strippedContents) {
				  
				// from url <u> to file in directory <path>
				// keep the file name
				String p =u.getFile();
				String f = p.substring(p.lastIndexOf("/")+1);
				  
				//if( downloadURL2(u,path + f)>0L ) 
				{
					done++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(done + " files downloaded");
		
	}

	private String downloadURL(String theURL){
		URL u;
		InputStream is = null;
		String s;
		StringBuffer sb = new StringBuffer();
		
		try{
		    u = new URL(theURL);
		    is = u.openStream();
		    
		    //depricated. DataInputStream dis = new DataInputStream(new BufferedInputStream(is));
		    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		    while ((s = reader.readLine()) != null){
		    	sb.append(s + "\n");
		    }
	    	is.close();
		} catch (Exception e){
		    e.printStackTrace();
		} finally {
		}
		return sb.toString();
	}

/*
 * import java.io.*;
 
/**
 * Copy one file to another using low level byte streams, one byte at a time.
 * @author www.codejava.net

public class CopyFiles {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Please provide input and output files");
            System.exit(0);
        }
 
        String inputFile = args[0];
        String outputFile = args[1];
 
 
        try (
            InputStream inputStream = new FileInputStream(inputFile);
            OutputStream outputStream = new FileOutputStream(outputFile);
        ) {
 
            int byteRead;
 
            while ((byteRead = inputStream.read()) != -1) {
                outputStream.write(byteRead);
            }
 
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
 */
	
	private long writeNote(String dir, String source){
	  
		long bytes = 0;
		try {
			PrintWriter writer = new PrintWriter(dir + "readme.txt", "UTF-8");
			writer.println("download from " + source);
			writer.close();
			
			/* binary output
			 * 
			 * 	byte data[] = ...
			 *	FileOutputStream out = new FileOutputStream("the-file-name");
			 *	out.write(data);
			 *	out.close();
			 */
			
		} catch (Exception e){
			e.printStackTrace();
		} finally {
	 	}
		
		System.out.println("from " + source + " to " + dir);
	  	return bytes;
	}

	private long downloadURL2(URL u,String outputFile){
		
		InputStream is = null;
		OutputStream outputStream = null;
			  
		//System.out.println("from " + u.toString() + " to " + outputFile);
	  
		long bytes = 0;
		try {
			is = u.openStream();
			outputStream = new FileOutputStream(outputFile);
	
			int byteRead = -1;
			while ((byteRead = is.read()) != -1) {
				outputStream.write(byteRead);
				bytes = bytes + byteRead;
			}
			
			is.close();
			outputStream.close();
			bytes = Files.size(new File(outputFile).toPath());
			
		} catch (Exception e){
			e.printStackTrace();
		} finally {
	 	}
		
		System.out.println("from " + u.toString() + " to " + outputFile + " bytes " + bytes);
	  	return bytes;
	}
	
	
	public String parseString(String s){
		String output = null;
	
		Pattern replaceWhitespacePattern = Pattern.compile("\\s");
		Matcher matcher = null;
		matcher = replaceWhitespacePattern.matcher(s);
		output = matcher.replaceAll(" ");
	
		Pattern removeHTMLTagsPattern = Pattern.compile("]*>");
		matcher = removeHTMLTagsPattern.matcher(output);
		output = matcher.replaceAll("");
	
		Pattern leaveOnlyAlphaNumericCharactersPattern = Pattern.compile("[^0-9a-zA-Z ]");
		matcher = leaveOnlyAlphaNumericCharactersPattern.matcher(output);
		output = matcher.replaceAll("");
	
		return output;
	}
	
	public List<URL> parseString2(String str){
		List<URL> list = new ArrayList<URL>();
		
		Pattern ptrn = Pattern.compile("imageURL\":\"");
		Matcher matcher = ptrn.matcher(str);
		while(matcher.find()){
		  
			String s2 = str.substring(matcher.end(), matcher.end()+1000);
			//int ind = s2.indexOf("}");
	
			int img = s2.indexOf("\"");
			//String s3 = str.substring(matcher.end(),matcher.end()+ind);
			
			
	
			String s4 = str.substring(matcher.end(),matcher.end()+img);
			
			s4 = s4.replaceAll("/","");
			
			try {
				
				//StringBuffer buf = new StringBuffer(s4);
				//for(int i=0; i<buf.length(); i++) {
				//	char cc = buf.charAt(i);			
				//	System.out.println(" char " + cc + " at " + i );
				//}
				
				
				
				  //Pattern removeHTMLTagsPattern = Pattern.compile("/s"); 
				  //matcher = removeHTMLTagsPattern.matcher(s4);
				  //s4 = matcher.replaceAll("\");
				
				s4 = s4.replaceAll("\\\\", "/");
				  System.out.println("Found ptrn index "+ matcher.start() +" - "+ (matcher.end()-1) + "  " + s4);
				
				
				list.add(new URL(s4));
	
				
			} catch (Exception e) {
				e.printStackTrace();
			}
	
		}
		return list;
	}
	
	//parse the page for 
	public List<URL> parseString3(String str,String source) throws MalformedURLException{
		List<String> list = new ArrayList<String>();
		
		
		//scripts
		list.addAll(fetchBy(str,"<script src=\""));
	
		//css
		List<String> list3 = fetchBy(str,"<link href=\"css/");
		for (int i = 0; i < list3.size(); i++) {
		    list3.set(i, "css/" + list3.get(i));
		}
		list.addAll(list3);


		List<URL> list2 = new ArrayList<URL>();
		for(String per: list) {
			String url = source + per;
			list2.add( new URL(url));
			System.out.println(url);
		}
		
		return list2; 
	}

	public List<String> fetchBy(String str, String pattern){
		
		List<String> list = new ArrayList<String>();
		
		Pattern ptrn2 = Pattern.compile(pattern);
		Matcher matcher2 = ptrn2.matcher(str);
		while(matcher2.find()){
		  
			String s2 = str.substring(matcher2.end(), matcher2.end()+1000);
			//int ind = s2.indexOf("}");
	
			int img = s2.indexOf("\"");
			//String s3 = str.substring(matcher.end(),matcher.end()+ind);
			
			
	
			String s4 = str.substring(matcher2.end(),matcher2.end()+img);
			
			//s4 = s4.replaceAll("/","");
			//s4 = "css//" + s4;
			
			try {
				
				//StringBuffer buf = new StringBuffer(s4);
				//for(int i=0; i<buf.length(); i++) {
				//	char cc = buf.charAt(i);			
				//	System.out.println(" char " + cc + " at " + i );
				//}
				
				
				
				  //Pattern removeHTMLTagsPattern = Pattern.compile("/s"); 
				  //matcher = removeHTMLTagsPattern.matcher(s4);
				  //s4 = matcher.replaceAll("\");
				
				//s4 = s4.replaceAll("\\\\", "/");
				System.out.println("Found ["+ matcher2.start() +" - "+ (matcher2.end()-1) + "] = " + s4);
								
				list.add(s4);
	
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}

}
