import java.text.SimpleDateFormat;
import java.util.Date;

import threadmonitor.ThreadInfo;
import threadmonitor.ThreadMonitor;

class TaskTwo extends Thread {
	//private String name; 
    
    public TaskTwo(String s) 
    { 
        setName(s); 
    }
    //get random
    //num=1 - 100% of true
    //num=2 - 50%
    //num=100 - 1% of true
	public static boolean getRand(int num) {
		return Math.random() < (1.0/(double)num) ? true : false;
	}
    public void run(){    
        
    	ThreadInfo info = new ThreadInfo(this);
    	ThreadMonitor.get().active(info);
    	
     	try
        { 
     		Date d = new Date(); 
     		SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss"); 
     		System.out.println("Initialization for"
     				+ " task name - "+ getName() +" - " +ft.format(d));   
     		
     		if(getRand(10)) {
        		System.out.println(" -- exception for"
         				+ " task name - "+ getName() +" = " +ft.format(d));   
     			throw new InterruptedException();
     		} 
                    
	       	if(getRand(5)) {
	        	System.out.println("-- long time initialization for"
	         				+ " task name - "+ getName() +" = " +ft.format(d));   
	     		Thread.sleep(10000);
	       	}

            Thread.sleep(1000); 
             
            System.out.println(getName()+" complete");
           	ThreadMonitor.get().completed(info);

            
        } catch(InterruptedException e) { 
            //e.printStackTrace(); 
         	ThreadMonitor.get().delayed(info);
          } 
    } 
}

