package urlutil;

import java.util.regex.Pattern;
import java.util.stream.Collectors;

import urls.SiteEnum;
import urls.SiteTemplate;
import urlutil.siteinfo.SiteFactory;

import java.util.ArrayList;
//import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;

public class ImageExtractor {
	
	String defaultName = "files";
	
	SiteTemplate st;
	
	SiteEnum se;

	public static void main(String[] args){
		//new JavaUrlExample(args[0]);
		for (String page:args) {
			
			if (!page.startsWith("\\$")) {
				System.out.println(page);
				new ImageExtractor(page);
			} else {
				System.out.println(page);
				break;
			}
			
		}
	}
	
	/*
	 * tempPath - destination
	 * name - template of directory name (name, name + "1" ... ,name + nn
	 */
	
	public String getDestination(String tempPath, String name) {
		String dir = null;
		boolean success = false;
		String	destName = null;
		
		if (name==null || name.isEmpty()) {
			name = "files";
		}
		
		if (tempPath!=null) {
			
			dir = tempPath.trim();
			
			dir = dir.replaceAll("\\\\", "/");
			
			if(!dir.endsWith("/")) {
				dir = dir + "/";
			}
		
			//for(int i=0; (i<50)&&!success; i++) {
			//	destName = tempPath + name + " " + i;
			destName = tempPath + name;
			int i = 0;
			while(!success) {
				File dest = new File(destName);
				if (dest.exists()) {
					destName = tempPath + name + " " + i;
	 			} else {
	 				success = dest.mkdirs(); // mkdir - one level
	 			}
			}
		}
			
		return success?destName+"/":null;
	}
	
	private String parseForTitle(String source) {
		
		//String content = downloadURL(source); 

		List<String> list = parseString2(source, "<meta name=\"twitter:title\" property=\"og:title\" content=\"","\"");
		
		return list.isEmpty()?defaultName:list.get(0);			
	}
	
	//java 8
	public List<URL> parseForURLs( String source) {
		
		return parseString2(source,"imageURL\":\"","\"").stream()
					.map(e -> {
						try {
							return new URL(e);
						} catch (MalformedURLException e1) {
							e1.printStackTrace();
							return null;
						}
					})
					.collect(Collectors.toList());

	}
	
	/*
	 * 
	 */
	// "imageURL\":\""
	
	private List<String> parseString2(String str, String anchor, String anchorEnd){
			
			List<String> list = new ArrayList<String>();
			
			Pattern ptrn = Pattern.compile(anchor);
			Matcher matcher = ptrn.matcher(str);
			
			while(matcher.find()){
			  
				String s2 = str.substring(matcher.end(), matcher.end()+1000);
				//int ind = s2.indexOf("}");
		
				int img = s2.indexOf(anchorEnd);
				//String s3 = str.substring(matcher.end(),matcher.end()+ind);
			
				String s4 = str.substring(matcher.end(),matcher.end()+img);
				
				s4 = s4.replaceAll("/","");
				
				try {
					
					//StringBuffer buf = new StringBuffer(s4);
					//for(int i=0; i<buf.length(); i++) {
					//	char cc = buf.charAt(i);			
					//	System.out.println(" char " + cc + " at " + i );
					//}
					
					
					
					  //Pattern removeHTMLTagsPattern = Pattern.compile("/s"); 
					  //matcher = removeHTMLTagsPattern.matcher(s4);
					  //s4 = matcher.replaceAll("\");
					
					s4 = s4.replaceAll("\\\\", "/");
					  System.out.println("Found ptrn index "+ matcher.start() +" - "+ (matcher.end()-1) + "  " + s4);
					
					
					//list.add(new URL(s4));
					list.add(s4);
		
					
				} catch (Exception e) {
					e.printStackTrace();
				}
		
			}
			return list;
	}
		

	
	// get path to eclipse running project
	// like C:\Users\Igor\eclipse-workspace\java\one-parent\one-mod\
	public String getTargetClassesPath() {
		
		//The resulting File object f represents the .jar file that was executed
		//.getURI fixes the issue with showing %20 instead of space 
		
		File classPath = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
		String runPath = classPath.getAbsolutePath();

		if (runPath.endsWith("target\\test-classes")) {
			runPath = runPath.substring(0,runPath.lastIndexOf("target\\test-classes"));
		}
		return runPath;
	}
	
	// get path to resources of eclipse running project
	// like C:\Users\Igor\eclipse-workspace\java\one-parent\one-mod\src\test\resources
	public String getResourcePath() {
		return (new File("src/test/resources")).getAbsolutePath();
	}
	
	/*
	 *  source - url to parse
	 *  path - path to save content 
	 */
	public ImageExtractor(String source){
	
		String path;
		//path = getTargetClassesPath();
		//path = getResourcePath();
		path = "C:\\Users\\Igor\\Downloads\\Ant\\pix\\ImageExtractor\\";
		
		String content = downloadURL(source);
		System.out.println("RAW CONTENTS:" + "\n" + content);
		
		se = SiteFactory.getInstance(source); //.getInstance(source);
		System.out.println(se.toString());
		
		st = SiteFactory.getSite(source); //SiteTemplate.getSite(source);
		
		// determine full destination path
		
		//path = getDestination(path,parseForTitle(content));  
		path = getDestination(path,st.parseForTitle(content));  
		
		System.out.println("Moving images from " + source + " to " + path);
		writeNote(path,source);
		System.out.println("Writing note to " + path + " with " + source);
		
		//List<URL> strippedContents = parseString2(content);
		List<URL> strippedContents = st.parseForURLs(content);
		
		//System.out.println("\nList of urls:" + "\n" + strippedContents);
		//Lambda Expression to print arraylist
		System.out.println("\nList of urls:");
		strippedContents.forEach((URL value) -> System.out.println(value.toString()));

	    long done = 0L;
	    
		try {
			for(URL u : strippedContents) {
				  
				// from url <u> to file in directory <path>
				// keep the file name
				String p = u.getFile();
				String f = p.substring(p.lastIndexOf("/")+1);
				  
				if( downloadURL2(u,path + f)>0L ) {
					done++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(done + " files downloaded");
		
	}

	private String downloadURL(String theURL){
		URL u;
		InputStream is = null;
		String s;
		StringBuffer sb = new StringBuffer();
		
		try{
		    u = new URL(theURL);
		    is = u.openStream();
		    
		    //depricated. DataInputStream dis = new DataInputStream(new BufferedInputStream(is));
		    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		    while ((s = reader.readLine()) != null){
		    	sb.append(s + "\n");
		    }
	    	is.close();
		} catch (Exception e){
		    e.printStackTrace();
		} finally {
		}
		return sb.toString();
	}

/*
 * import java.io.*;
 
/**
 * Copy one file to another using low level byte streams, one byte at a time.
 * @author www.codejava.net

public class CopyFiles {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Please provide input and output files");
            System.exit(0);
        }
 
        String inputFile = args[0];
        String outputFile = args[1];
 
 
        try (
            InputStream inputStream = new FileInputStream(inputFile);
            OutputStream outputStream = new FileOutputStream(outputFile);
        ) {
 
            int byteRead;
 
            while ((byteRead = inputStream.read()) != -1) {
                outputStream.write(byteRead);
            }
 
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
 */
	
	private long writeNote(String dir, String source){
	  
		long bytes = 0;
		try {
			PrintWriter writer = new PrintWriter(dir + "readme.txt", "UTF-8");
			writer.println("download from " + source);
			writer.close();
			
			/* binary output
			 * 
			 * 	byte data[] = ...
			 *	FileOutputStream out = new FileOutputStream("the-file-name");
			 *	out.write(data);
			 *	out.close();
			 */
			
		} catch (Exception e){
			e.printStackTrace();
		} finally {
	 	}
		
		System.out.println("from " + source + " to " + dir);
	  	return bytes;
	}

	private long downloadURL2(URL u,String outputFile){
		
		InputStream is = null;
		OutputStream outputStream = null;
			  
		//System.out.println("from " + u.toString() + " to " + outputFile);
	  
		long bytes = 0;
		try {
			is = u.openStream();
			outputStream = new FileOutputStream(outputFile);
	
			int byteRead = -1;
			while ((byteRead = is.read()) != -1) {
				outputStream.write(byteRead);
				bytes = bytes + byteRead;
			}
			
			is.close();
			outputStream.close();
			bytes = Files.size(new File(outputFile).toPath());
			
		} catch (Exception e){
			e.printStackTrace();
		} finally {
	 	}
		
		System.out.println("from " + u.toString() + " to " + outputFile + " bytes " + bytes);
	  	return bytes;
	}
	
	
	public String parseString(String s){
		String output = null;
	
		Pattern replaceWhitespacePattern = Pattern.compile("\\s");
		Matcher matcher = null;
		matcher = replaceWhitespacePattern.matcher(s);
		output = matcher.replaceAll(" ");
	
		Pattern removeHTMLTagsPattern = Pattern.compile("]*>");
		matcher = removeHTMLTagsPattern.matcher(output);
		output = matcher.replaceAll("");
	
		Pattern leaveOnlyAlphaNumericCharactersPattern = Pattern.compile("[^0-9a-zA-Z ]");
		matcher = leaveOnlyAlphaNumericCharactersPattern.matcher(output);
		output = matcher.replaceAll("");
	
		return output;
	}
	
	public List<URL> parseString2(String str){
		List<URL> list = new ArrayList<URL>();
		
		Pattern ptrn = Pattern.compile("imageURL\":\"");
		Matcher matcher = ptrn.matcher(str);
		while(matcher.find()){
		  
			String s2 = str.substring(matcher.end(), matcher.end()+1000);
			//int ind = s2.indexOf("}");
	
			int img = s2.indexOf("\"");
			//String s3 = str.substring(matcher.end(),matcher.end()+ind);
			
			
	
			String s4 = str.substring(matcher.end(),matcher.end()+img);
			
			s4 = s4.replaceAll("/","");
			
			try {
				
				//StringBuffer buf = new StringBuffer(s4);
				//for(int i=0; i<buf.length(); i++) {
				//	char cc = buf.charAt(i);			
				//	System.out.println(" char " + cc + " at " + i );
				//}
				
				
				
				  //Pattern removeHTMLTagsPattern = Pattern.compile("/s"); 
				  //matcher = removeHTMLTagsPattern.matcher(s4);
				  //s4 = matcher.replaceAll("\");
				
				s4 = s4.replaceAll("\\\\", "/");
				  System.out.println("Found ptrn index "+ matcher.start() +" - "+ (matcher.end()-1) + "  " + s4);
				
				
				list.add(new URL(s4));
	
				
			} catch (Exception e) {
				e.printStackTrace();
			}
	
		}
		return list;
	}


}
