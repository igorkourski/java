package urlutil.siteinfo;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

import urls.SiteEnum;
import urls.SiteTemplate;

public class SiteFirst extends SiteTemplate {

	public SiteFirst(SiteEnum site, String source) {
		super(site, source);
	}

	@Override
	public String parseForTitle(String source) {

		List<String> list = SiteTemplate.parseString2(source, site.getTitleAnchor(),site.getTitleAnchorEnd());
		return list.isEmpty() ? defaultName : list.get(0);			

	}

	@Override
	public List<URL> parseForURLs(String source) {
	
			return SiteTemplate.parseString2(source,site.getUrlAnchor(),site.getUrlAnchorEnd()).stream()
						.map(e -> {
							try {
								return new URL(e);
							} catch (MalformedURLException e1) {
								e1.printStackTrace();
								return null;
							}
						})
						.collect(Collectors.toList());
	}

}
