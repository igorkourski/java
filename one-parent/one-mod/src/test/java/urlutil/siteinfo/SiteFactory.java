package urlutil.siteinfo;

import java.util.Arrays;
import java.util.List;

import urls.SiteEnum;
import urls.SiteTemplate;

public class SiteFactory {

	//site distinguisher 
	final static SiteEnum[] list = {
			
			new SiteEnum(
					"/photos/gallery/",
					"<meta name=\"twitter:title\" property=\"og:title\" content=\"",
					"\"",
					"imageURL\":\"",
					"\""),
			
			new SiteEnum(
					"2read.com/",
					"<span itemprop=\"name\">",
					"</scan>",
					"",
					""),
			
			new SiteEnum(
					"comics.com/book",
					"Book id: #",
					".</title>",
					"<meta property=\"og:image\" content=\"",
					"\"")
			
	};
	
	static List<SiteEnum> list2 = Arrays.asList(list);
	
	//static
	public static SiteEnum getInstance(String source) {
		
		if (source!=null) {
			
			for(SiteEnum site:list) {
			
				if ( source.contains(site.getKey()) ) {
					return site;
				}
			}
			
		}
		
		return null;
	}
	
	public static SiteTemplate getSite(String source) {
		
		SiteEnum site = getInstance(source);
		
		if (site==null) {
			
			return null;
			
		} else {
			
			if (site.getKey().equalsIgnoreCase(list[0].getKey())){
			
				return new SiteFirst(site,source);
			}
			
			if (site.getKey().equalsIgnoreCase(list[1].getKey())){
				
				return new SiteSecond(site,source);
			}
			
			if (site.getKey().equalsIgnoreCase(list[2].getKey())){
				
				return new SiteThird(site,source);
			}
			
			return null;
			
		}
	}
	
	
}
