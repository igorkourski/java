package urlutil.siteinfo;

import java.net.URL;
import java.util.List;

import urls.SiteEnum;
import urls.SiteTemplate;

public class SiteSecond extends SiteTemplate {

	public SiteSecond(SiteEnum site, String source) {
		super(site, source);
	}

	@Override
	public String parseForTitle(String source) {
		List<String> list = SiteTemplate.parseString2(source, site.getTitleAnchor(),site.getTitleAnchorEnd());
		return list.isEmpty() ? defaultName : list.get(0);			

	}

	@Override
	public List<URL> parseForURLs(String source) {


		
		return null;
	}

}
