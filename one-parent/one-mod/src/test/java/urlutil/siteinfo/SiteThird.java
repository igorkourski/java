package urlutil.siteinfo;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import urls.SiteEnum;
import urls.SiteTemplate;

public class SiteThird extends SiteTemplate {

	public SiteThird(SiteEnum site, String source) {
		super(site, source);
	}

	@Override
	public String parseForTitle(String source) {
		List<String> list = SiteTemplate.parseString2(source, site.getTitleAnchor(),site.getTitleAnchorEnd());
		return list.isEmpty() ? defaultName : list.get(0);			

	}

	@Override
	public List<URL> parseForURLs(String source) {
		
		return parse(source).stream()
				.map(e -> {
					try {
						return new URL(e);
					} catch (MalformedURLException e1) {
						e1.printStackTrace();
						return null;
					}
				})
				.collect(Collectors.toList());
	}
	
	public List<String> parse(String source) {
		
		//http://www.azporncomics.com/book/49604/1.html
		
		List<String> result = new ArrayList<String>();
		
		List<String> list = parseString2(source, site.getUrlAnchor(),site.getUrlAnchorEnd());
		
		result.addAll(list);
		
		boolean done=false;
		while(!done) {
		
			List<String> list2 = parseString2(source, "<div class=\"pic1\"><a href=\"","\"");
			
			if(!list2.isEmpty()) {
				
				String page = list2.get(0);
				if( page.contains("index.html") ) {
					done = true;
				} else {
					String content = downloadURL(list2.get(0));
					List<String> list3 = parse(content);
					result.addAll(list3);
					done = true;
				}
			} else {
				done = true;
			}
			
			
		}
	
		return result;
	}
	
	public static List<String> parseString2(String str, String anchor, String anchorEnd){
		
		List<String> list = new ArrayList<String>();
		
		Pattern ptrn = Pattern.compile(anchor);
		Matcher matcher = ptrn.matcher(str);
		
		while(matcher.find()){
		  
			String s2 = str.substring(matcher.end(), matcher.end()+1000);
			//int ind = s2.indexOf("}");
	
			int img = s2.indexOf(anchorEnd);
			//String s3 = str.substring(matcher.end(),matcher.end()+ind);
		
			String s4 = str.substring(matcher.end(),matcher.end()+img);
			
			//s4 = s4.replaceAll("/","");
			
			try {
				
				//StringBuffer buf = new StringBuffer(s4);
				//for(int i=0; i<buf.length(); i++) {
				//	char cc = buf.charAt(i);			
				//	System.out.println(" char " + cc + " at " + i );
				//}
				
				
				
				  //Pattern removeHTMLTagsPattern = Pattern.compile("/s"); 
				  //matcher = removeHTMLTagsPattern.matcher(s4);
				  //s4 = matcher.replaceAll("\");
				
				//s4 = s4.replaceAll("\\\\", "/");
				System.out.println("Found ptrn index "+ matcher.start() +" - "+ (matcher.end()-1) + "  " + s4);
				
				
				//list.add(new URL(s4));
				list.add(s4);
	
				
			} catch (Exception e) {
				e.printStackTrace();
			}
	
		}
		return list;
	}

}
