package collections;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

public class SingletonQueueTest extends Object implements Map{

	@Test
	public void test() {
		SingletonQueue testQueue = SingletonQueue.getStreamInstance();
		 
		testQueue.add("Crunchify");
		testQueue.add("Google");
		testQueue.add("Yahoo");
		testQueue.add("Facebook");
		System.out.println("Initial Queue: " + testQueue.get());
 
		testQueue.remove("Google");
		System.out.println("After removing Google: " + testQueue.get());
 
		System.out.println("totalSize: " + testQueue.getTotalSize());
		System.out.println("isEmpty(): " + testQueue.isEmpty());
 
		System.out.println("poll(): " + testQueue.poll());
		System.out.println("After Polling: " + testQueue.get());
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean containsKey(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsValue(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Set entrySet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object get(Object arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Set keySet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object put(Object arg0, Object arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void putAll(Map arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object remove(Object arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Collection values() {
		// TODO Auto-generated method stub
		return null;
	}

}
