package collections;

import static org.junit.Assert.*;

import org.junit.Test;

public class LinkedListTest {

	@Test
	public void test() {
		LinkedList<Integer> list = new LinkedList<Integer>();
		 
		int randomNo;
 
		println("in main()...\n");
		// Let's create integer array with 8 values in it
		for (int i = 1; i <= 8; i++) {
			randomNo = (10 + (int) (Math.random() * ((55 - 2)))); 
			println("Adding Element: " + randomNo);
			list.addElement(randomNo);
		}
 
		println("\nOriginal Linked List:");
		println(list.toString());
 
		println("Reversing");
		list.reverse();
 
		println("Reversed Linked List:");
		println(list.toString());
	}

	// Simple Log utility with new line
	private static void println(String string) {
		System.out.println(string);
	}

}
