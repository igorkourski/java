package codility;

import java.util.HashMap;
import java.util.Map;

/*
 * 

An array A consisting of N different integers is given. The array contains integers in the range [1..(N + 1)], which means that exactly one element is missing.

Your goal is to find that missing element.

Write a function:

    function solution(A)

that, given an array A, returns the value of the missing element.

For example, given array A such that:
  A[0] = 2
  A[1] = 3
  A[2] = 1
  A[3] = 5

the function should return 4, as it is the missing element.

Write an efficient algorithm for the following assumptions:

        N is an integer within the range [0..100,000];
        the elements of A are all distinct;
        each element of array A is an integer within the range [1..(N + 1)].

Note: All arrays in this task are zero-indexed, unlike the common Lua convention. You can use #A to get the length of the array A.

 */

public class Solution4_PermMissEl {
	
	public static int solution(int[] A) {
		int result = -1;
		
		Map m = new HashMap();
		int var = 0;
		for(int iper:A) {
			m.put(iper,iper);
		}
		for(int i=1; i<=A.length+1; i++) {
			if( m.containsKey(i)) {
				
			} else {
				return i;
			}
		}
		
		return result;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] A = {2,3,5,1};
		int iper = solution(A);
		
		System.out.println("done " + iper);
	}

}
