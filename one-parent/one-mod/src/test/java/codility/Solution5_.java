package codility;

/*
 * 

A non-empty array A consisting of N integers is given. The consecutive elements of array A represent consecutive cars on a road.

Array A contains only 0s and/or 1s:

        0 represents a car traveling east,
        1 represents a car traveling west.

The goal is to count passing cars. We say that a pair of cars (P, Q), where 0 ≤ P < Q < N, is passing when P is traveling to the east and Q is traveling to the west.

For example, consider array A such that:
  A[0] = 0
  A[1] = 1
  A[2] = 0
  A[3] = 1
  A[4] = 1

We have five pairs of passing cars: (0, 1), (0, 3), (0, 4), (2, 3), (2, 4).

Write a function:

    class Solution { public int solution(int[] A); }

that, given a non-empty array A of N integers, returns the number of pairs of passing cars.

The function should return −1 if the number of pairs of passing cars exceeds 1,000,000,000.

For example, given:
  A[0] = 0
  A[1] = 1
  A[2] = 0
  A[3] = 1
  A[4] = 1

the function should return 5, as explained above.

Write an efficient algorithm for the following assumptions:

        N is an integer within the range [1..100,000];
        each element of array A is an integer that can have one of the following values: 0, 1.


 */
public class Solution5_ {
	
	static int ps[];
	static int n;
	
	public static int solution0(int[] A) {
	    int result = 0;
		for(int i=0; i<A.length; i++) {
			if (A[i]==0)
			for(int j=i+1; j<A.length; j++) {
			
				if (A[i]!=A[j]) {
					//System.out.println("(" + i + "/" + j + ")="+ A[i] + " " + A[j]);
					result++;
					if( result>1000000000 ){
						return -1;
					}
				}
			}
		}
		return result;
	}
	public static int total(int x,int y) {
		// 0<=x<=y<n
		if ( !((0<=x) && (x<=y) && (y<n)) ) {
			return 0;
		}
		return ps[y+1] - ps[x];
	}
	
	public static int solution(int[] A) {
	    int result = 0;
	    
	    //init prefix sums
	    n = A.length+1;
	    ps = new int[n];  //0...n
	    ps[0] = 0;
	    for(int i=1; i<n; i++) {
			ps[i] = ps[i-1] + A[i-1];
		}
    
	    //
		for(int i=0; i<A.length; i++) {
			if (A[i]==0) {
				result = result + total(i+1,A.length-1);
				if( result>1000000000 ){
					return -1;
				}
			}
			
		}
		return result;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] A = {0,1,0,1,1};
		
		int iper = solution(A);
		
		System.out.println(iper); //5
		
		/*
		int size = 2000000;
		int[] b = new int[size];
		for(int k=0; k<size/10; k++ ) {
			b[k*10] = 1;
		}
		iper = solution(b);
		*/
		int iper2 = solution(new int[] {1}); //0
		int iper2a = solution(new int[] {0}); //0
		
		int iper3 = solution(new int[] {0,1}); //1
		int iper4 = solution(new int[] {1,0}); //0
		int iper5 = solution(new int[] {1,1}); //0
		int iper6 = solution(new int[] {0,0}); //0
		
		
		System.out.println(iper);
	}

}
