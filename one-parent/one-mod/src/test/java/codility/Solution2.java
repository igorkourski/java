package codility;

public class Solution2 {


/*
A binary gap within a positive integer N is any maximal sequence of consecutive zeros that is surrounded by ones at both ends in the binary representation of N.

For example, number 9 has binary representation 1001 and contains a binary gap of length 2. 
The number 529 has binary representation 1000010001 and contains two binary gaps: one of length 4 and one of length 3. 
The number 20 has binary representation 10100 and contains one binary gap of length 1. 
The number 15 has binary representation 1111 and has no binary gaps. 
The number 32 has binary representation 100000 and has no binary gaps.

Write a function:

    class Solution { public int solution(int N); }

that, given a positive integer N, returns the length of its longest binary gap. 
The function should return 0 if N doesn't contain a binary gap.

For example, given N = 1041 the function should return 5, because N has binary representation 10000010001 
and so its longest binary gap is of length 5. 
Given N = 32 the function should return 0, because N has binary representation '100000' and thus no binary gaps.

Write an efficient algorithm for the following assumptions:

        N is an integer within the range [1..2,147,483,647].
        */
	public static int solution(int N) {
		
		String binary=Integer.toBinaryString(N);
		
		int start = -1;
		int gap = -1;
		int maxgap = 0;
		
		for(int i=0; i<binary.length(); i++) {
			
			char element = binary.charAt(i);
			
			if(element=='1') {
				if(start<0) {
					start = i;
				} else {
					gap = i-start-1;
					
					if (maxgap<gap) {
						maxgap = gap;
					}
					
					start = i;
				}
			}
			

			
		}
		
		
		return maxgap;

		
	}
	
	public static void main(String[] args) {
		
		int iper1 = solution(529);
		
		int iper2 = solution(20);
		
		int iper3 = solution(15);
		
		int iper4 = solution(32);
		
		assert(iper1==4);
		assert(iper2==1);
		assert(iper3==0);
		assert(iper4==0);
		assert(solution(1041)==5);
		
		System.out.println("success");
		
		
	}


}
