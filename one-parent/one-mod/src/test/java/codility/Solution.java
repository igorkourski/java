package codility;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Solution {

/*
	given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.
	
	For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

			Given A = [1, 2, 3], the function should return 4.

			Given A = [−1, −3], the function should return 1.

			Write an efficient algorithm for the following assumptions:

			        N is an integer within the range [1..100,000];
			        each element of array A is an integer within the range [−1,000,000..1,000,000].
*/

	public static int solution(int[] A) {
		// write your code in Java SE 8
		/*
		int result = 1;
		
		Arrays.sort(A);
		
		for( int i : A) {
			if (i == result) {
				
			}
		}
		
	    for(int result=1 ; result<Integer.MAX_VALUE; result++ ) {
	        	
	        	for(int iper)
	        	
	        }
	    */
	    Map m = new HashMap();
	    for( int iper: A) {
	    	m.put(iper, iper);
	    }
	    for (int result=1; result<Integer.MAX_VALUE; result++) {
	    	
	    	Object o = m.put(result,result);
	    	if (o==null) {
	    		return result;
	    	}
	    	
	    }
	    return 1;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] set =  {1, 3, 6, 4, 1, 2};
		int[] set2 = {};
		int[] set3 = {-1,-3};
		int[] set4 = {1,2,3};
		
		int iper1 = solution(set);
		int iper2 = solution(set2);
		int iper3 = solution(set3);
		int iper4 = solution(set4);

		System.out.println(iper1 + " " + iper2 + " " + iper3 + " " + iper4);
		
		
	}

}
