package codility;

/*
 * An array A consisting of N integers is given. 
 * Rotation of the array means that each element is shifted right by one index, 
 * and the last element of the array is moved to the first place. 
 * For example, the rotation of array A = [3, 8, 9, 7, 6] is [6, 3, 8, 9, 7] (elements are shifted right by one index and 6 is moved to the first place).

The goal is to rotate array A K times; that is, each element of A will be shifted to the right K times.
 */

public class Solution3_Array {
	
	
	public static int[] solution(int[] A, int K) {

		int L = A.length;
				
		if (K > L) {
			K = K % L;
		}
		
		if(K==0 || L==0 || L==1 ) {
			return A;
		}
		
		int[] result = new int[L];
		
		for(int i=0; i<L; i++) {
			
			//new index
			int index = i + K;
			
			if (index>L-1) {
				index = index - L;
			}
			
			result[index] = A[i];
			
		}
		
		return result;
	}
	
	public static boolean compare(int[] a, int[] b) {
		if (a.length!=b.length)
			return false;
		for(int i=0; i<a.length; i++) {
			if(a[i]!=b[i])
				return false;
			
		}
		return true;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] A = {3, 8, 9, 7, 6};
		int    K = 1;
		int [] B = {6, 3, 8, 9, 7};
		int [] C = solution(A,K);
		//assert(compare(solution(A,K),B));

		int [] A2 = {3, 8, 9, 7, 6};
		K = 3;
		int [] B2 = {9, 7, 6, 3, 8};
		int [] C2 = solution(A2,K);
		//assert(compare(solution(A2,K),B2));
		
		int [] A3 = {1, 2, 3, 4};
		K = 4;
		int [] B3 = {1, 2, 3, 4};
		int [] C3 = solution(A3,K);
		assert(compare(solution(A3,K),B3));
		
		System.out.println("done");
		
	}

}
