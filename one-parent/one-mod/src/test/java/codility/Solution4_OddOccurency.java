package codility;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/*
 * A non-empty array A consisting of N integers is given. 
 * The array contains an odd number of elements, and each element of the array can be paired with another element that has the same value, except for one element that is left unpaired.

For example, in array A such that:
  A[0] = 9  A[1] = 3  A[2] = 9
  A[3] = 3  A[4] = 9  A[5] = 7
  A[6] = 9

        the elements at indexes 0 and 2 have value 9,
        the elements at indexes 1 and 3 have value 3,
        the elements at indexes 4 and 6 have value 9,
        the element at index 5 has value 7 and is unpaired.

Write a function:
    int solution(int A[], int N);

that, given an array A consisting of N integers fulfilling the above conditions, returns the value of the unpaired element.

For example, given array A such that:
  A[0] = 9  A[1] = 3  A[2] = 9
  A[3] = 3  A[4] = 9  A[5] = 7
  A[6] = 9

the function should return 7, as explained in the example above.

Write an efficient algorithm for the following assumptions:

        N is an odd integer within the range [1..1,000,000];
        each element of array A is an integer within the range [1..1,000,000,000];
        all but one of the values in A occur an even number of times.


 */

public class Solution4_OddOccurency {

	public static int solution0(int A[]) {
		int result = 0;
		Map<Integer,Integer> m = new HashMap<Integer,Integer>();
		
		Map<Integer,Integer> m1 = new HashMap<Integer,Integer>();
		
		for(int iper: A) {
			
			Object var = m.put(Integer.valueOf(iper), Integer.valueOf(iper));
			
			if(var==null) {
				m1.put(iper, iper);
			} else {
				m1.remove(iper);
			}
		}
		
		if (m1.isEmpty() || m1.size()>1) {
			result = 0;
		} else {
			if(A.length==1) {
				result = 0;
			} else {
				Iterator<Integer> it = m1.keySet().iterator();
				result = it.next().intValue();
			}
		}
		
		return result;
	}
	
	public static int solution(int A[]) {
		int result = 0;
		Map<Integer,Integer> m = new HashMap<Integer,Integer>();
		
		for(int iper: A) {
			
			if (m.containsKey(Integer.valueOf(iper))){
				m.put(Integer.valueOf(iper), m.get(Integer.valueOf(iper))+1);
			} else {
				m.put(Integer.valueOf(iper), Integer.valueOf(1));
			}
		}
		for(Integer key: m.keySet()) {
		
			Integer iper = m.get(key);
			if ( (iper%2)!= 0 ) {
				return key.intValue();
			}
		}

		return result;
	}
	
	public static void main(String[] args) {
	
		int[] A = {9,3,9,3,9,7,9};
		
		assert(solution(A)==7);
		
		assert(solution(new int[]{})==0); 
		
		assert(solution(new int[] {42})==42); //42
		
		assert(solution(new int[] {1,1,2})==2); //2
		
		System.out.println("done");
	
	}
}
		