package userinput;

import static org.junit.Assert.*;

import org.junit.Test;

	/*
	 * 
	 * One capital one low one number
	
	AAA)  ^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$
	
			descriptions are as follow
			(?=.*[a-z])  -- check lower case letter
			(?=.*[A-Z]) -- check upper case letter
			(?=.*\d) -- check one digit existsone 
	  
	 BBB) public final Pattern textPattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$");
	
	public boolean isTextValid(String textToCheck) {
	return textPattern.matcher(textToCheck).matches();
	}
	*/
	//optional 
	/*
	// to emulate contains, [a-z] will fail on more than one character, 
	// so you must add .* on both sides.
	if (s.matches(".*[a-z].*")) { 
    	return AtLeastOneLetter
	}
	
	do check if all characters are a-z use:
	if ( ! s.matches(".*[^a-z].*") ) { 
    	return ExistNotLetter;
	}
	return OnlyLetters
	
	public boolean isLettersOnly(String str) {
		if (str==null)
			return false;
		String s = str;
		
		if (s.matches(".*[^a-z].*")) { 
		    return true;
		}
		return false;
	}
	*/
	/////////////////////




public class password {
	
	public static boolean validation(String password) {
		
		if (password==null)
			return false;
		
		if (password.trim().length()<6)
			return false;
		
		//at least 1 upper and 1 low and 1 number
		boolean capitalFlag = false;
		boolean lowerCaseFlag = false;
		boolean numberFlag = false;
	    
		for (char ch : password.toCharArray()) {
			
		
			if( Character.isDigit(ch)) {
	            numberFlag = true;
			} else if (Character.isUpperCase(ch)) {
	            capitalFlag=true;
	        } else if (Character.isLowerCase(ch)) {
	            lowerCaseFlag = true;
	        }
			
		}
		if(numberFlag && capitalFlag && lowerCaseFlag) 
			;
		else
			return false;
		////////////////////////
		
		
		
		//char [] vowel = {'A','E','O','U'};
		//char [] consonant = {'B', 'C', 'D', 'F', 'G', 'H'};
		boolean isVowel = false;
		boolean isConsent = false;
		for(char c: password.toCharArray()) {
			
			if (!Character.isDigit(c)) {
				if(isVowel(c))
					isVowel = true;
				if(isConsent(c))
					isConsent = true;
			}
			
		}
		if (!isVowel && !isConsent)
			return false;
		
		//
		
		
		//not triple consecutive vowels or consonants,
		int n = password.length();
		for(int i=0; i<n; i++) {
			
			char a = password.charAt(i);
			
			char c = Character.toLowerCase(a);
			
			
			
			if (Character.isLetter(c)) {
			
				if (i+2<n) {
	
					char c2 = password.charAt(i+1);
					char c3 = password.charAt(i=2);
							
					if (isVowel(c) && isVowel(c2) && isVowel(c3)) {
						return false;
					}
					if (isConsent(c) && isConsent(c) && isConsent(c3)) {
						return false;
					}
					i++; //???
				}
				
				if (i+1<n) {
					char c2 = password.charAt(i+1);
					if (c==c2 && ((c!='e') || (c!='o')) ){
						return false;
					}
				}
			
			}
			
		}
		//no double consecutive letters except for "ee" and "oo"   
		
		
		return true;
	}
	
	//optional 
	/*
	// to emulate contains, [a-z] will fail on more than one character, 
	// so you must add .* on both sides.
	if (s.matches(".*[a-z].*")) { 
    	return AtLeastOneLetter
	}
	
	do check if all characters are a-z use:
	if ( ! s.matches(".*[^a-z].*") ) { 
    	return ExistNotLetter;
	}
	return OnlyLetters
	*/
	public boolean isLettersOnly(String str) {
		if (str==null)
			return false;
		String s = str;
		
		if (s.matches(".*[^a-z].*")) { 
		    return true;
		}
		return false;
	}
	/////////////////////
	
	public static boolean isVowel(char c) {
		char ch = Character.toUpperCase(c);
		
		if( Character.isDigit(ch))
			return false;
		
		char [] vowel = {'A','E','O','U'};
		
		for( char v: vowel) {
			
			if (ch==v)
				return true;
		}
		return false;
	}
	public static boolean isConsent(char c) {
		
		if( Character.isDigit(c))
			return false;
		
		return !isVowel(c);
	}
	
	
	
	@Test
	public void test() {
		
		assertEquals(true,validation("Aa11224B"));
		
		assertEquals(true,validation("333"));
		
	}

}
