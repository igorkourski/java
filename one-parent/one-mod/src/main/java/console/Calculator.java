package console;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//import java.awt.*;
//import java.awt.event.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import calculator_mvc.Controller;
import calculator_mvc.Model;


public class Calculator extends JFrame implements ActionListener{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JTextField text = new JTextField("text");
	
    JButton button1 = new JButton("1");
    JButton button2 = new JButton("2");
	JButton button3 = new JButton("3");
    JButton button4 = new JButton("4");
	JButton button5 = new JButton("5");
    JButton button6 = new JButton("6");
	JButton button7 = new JButton("7");
    JButton button8 = new JButton("8");
	JButton button9 = new JButton("9");
    
	JButton button0 = new JButton("0");
    JButton buttonDOT = new JButton(".");
    JButton buttonEQ = new JButton("=");
    
    JButton buttonCE  = new JButton("CE");
    JButton buttonDIV = new JButton("/");
    JButton buttonMUL = new JButton("x");
    JButton buttonSB  = new JButton("-");
    JButton buttonPL  = new JButton("+");
    
    
	
	public Calculator() {
        initUI();
    }

    public final void initUI() {
    	text.setBounds(10,10,155,30);
    	text.addActionListener(this);
    	text.setText("");
    	getContentPane().add(text);
    	
    	
        button7.setBounds(10,  45, 45, 45);  //7,8,9
        button8.setBounds(60,  45, 45, 45);
        button9.setBounds(110, 45, 45, 45);
        
        button4.setBounds(10,  95, 45, 45); //4,5,6
        button5.setBounds(60,  95, 45, 45);
        button6.setBounds(110, 95, 45, 45);
        
        button1.setBounds(10, 145, 45, 45); //1,2,3
        button2.setBounds(60, 145, 45, 45);
        button3.setBounds(110,145, 45, 45);
        
        button0	  .setBounds(10, 195, 45, 45);
        buttonDOT .setBounds(60, 195, 45, 45);
        buttonEQ  .setBounds(110, 195, 45, 45);
        
        buttonCE  .setBounds(160, 1,  45, 45);
        buttonDIV .setBounds(160, 45, 45, 45);
        buttonMUL .setBounds(160, 95, 45, 45);
        buttonSB  .setBounds(160, 145, 45, 45);
        buttonPL  .setBounds(160, 195, 45, 45);
        
        
		button1.addActionListener(this);
		button2.addActionListener(this);
		button3.addActionListener(this);
		button4.addActionListener(this);
		button5.addActionListener(this);
		button6.addActionListener(this);
		button7.addActionListener(this);
		button8.addActionListener(this);
		button9.addActionListener(this);
		button0.addActionListener(this);
		
	    button0.addActionListener(this);
	    buttonDOT.addActionListener(this);
	    buttonEQ.addActionListener(this);
	        
	    buttonCE .addActionListener(this);
	    buttonDIV.addActionListener(this);
	    buttonMUL.addActionListener(this);
	    buttonSB .addActionListener(this);
	    buttonPL .addActionListener(this);
        
        
        JPanel panel = new JPanel();
        getContentPane().add(panel);
        panel.setToolTipText("A Panel container");
        getContentPane().setLayout(null);
        
		getContentPane().add(button1);
        getContentPane().add(button2);
		getContentPane().add(button3);
        getContentPane().add(button4);
		getContentPane().add(button5);
        getContentPane().add(button6);
		getContentPane().add(button7);
        getContentPane().add(button8);
		getContentPane().add(button9);
        getContentPane().add(button0);
        
        getContentPane().add(buttonDOT);
        getContentPane().add(buttonEQ);
		getContentPane().add(buttonCE);
        getContentPane().add(buttonDIV);
		getContentPane().add(buttonMUL);
        getContentPane().add(buttonSB);
		getContentPane().add(buttonPL);
        
        
        
        
        
		setSize(230, 290);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    public void actionPerformed(ActionEvent e){
    	
    	String txt = text.getText();
    	String act = e.getActionCommand();
		boolean isDouble = txt.indexOf(".")>=0;
    	Model.Operation operation = null;

		try {
			operation = Model.Operation.DIGIT;
			int num = Integer.parseInt(act);
			Model.getModel().setLastInput(num);
				
		} catch (NumberFormatException e1) {

			//switch(act) {
				/*
				case "CE":	
					String txt2 = txt.substring(0, txt.length()-1);
					text.setText(txt2);
					break;
				case ".":
					if (isDouble) {
						
					} else {
						text.setText(txt + ".");
					}
				default:
				*/ 
					//text.setText(text.getText() + e.getActionCommand());

			    	switch(e.getActionCommand()) {
			    		case "CE":  operation = Model.Operation.CLEAR; break;
			    		case "/":	operation = Model.Operation.DIVIDE; break;
			    		case "x":	operation = Model.Operation.MULTIPLY; break;
			    		case "-":	operation = Model.Operation.SUBTRACT; break;
			    		case "+":   operation = Model.Operation.ADD; break;
			    		case "=":   operation = Model.Operation.EQUAL; break;
			    		case ".":	operation = Model.Operation.DOT; break;
			    		case "text":
			    		default: break;
			    	}

			    	
			//
		}
	    
		Model.getModel().setOperation(operation);
	    		
		txt = Controller.getDisplay();
		
		Model.getModel().setDisplay(txt);
		text.setText(txt);
			
		System.out.println("action=" + act + " display=" + txt);
	}
    
    public static void main(String[] args) {
        Calculator ex = new Calculator();
        ex.setVisible(true);
    }
}
