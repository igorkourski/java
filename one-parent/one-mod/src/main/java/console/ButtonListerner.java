package console;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class ButtonListener implements ActionListener{
	public ButtonListener(String msg){
		this.msg = msg;
	}
	public void actionPerformed(ActionEvent e){
		System.out.println(msg);
	}
	String msg;
}
