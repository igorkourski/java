package console;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class testUI extends JFrame {

    public testUI() {
    	setTitle("Simple example");
    	setSize(300, 200);
    	setLocationRelativeTo(null);
    	setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
    	testUI ex = new testUI();
    	ex.setVisible(true);
    }
}
