package calculator_mvc;

import calculator_mvc.Model.Operation;

public class Controller {
	
	public static String getDisplay() {
		
		String result = "";
		Model 	model = Model.getModel();
		String 	txt = model.getDisplay();
		int		step = model.getStep();
		
		//step 0
		if (step==0 || step==1) {
		
			switch(model.getOperation()) {
				case DIGIT:
					result = txt + model.getLastInput();
					break;
				case CLEAR:
					result = txt.substring(0, txt.length()-1);
					break;
				case DOT:
					if(txt.indexOf(".")>0) {
						//
					} else {
						result = txt + ".";
					}
					break;
				case DIVIDE:
				case MULTIPLY:
				case ADD:
				case SUBTRACT:
					if (step==0) {
						model.setStep(model.getStep()+1);
						model.setPrevious(model.getOperation());
						model.setMemory(txt);
						model.setDisplay("");
					} else
					if(step==1) {
						result = calculate();
						model.setStep(0);
						model.setPrevious(model.getOperation());
						model.setMemory(result);
						model.setDisplay("");
					}
					break;
				case EQUAL:
					if(step==1) {
						result = calculate();
						model.setStep(0);
						model.setDisplay(result);
					}
					break;
				default:
					break;
			}
			
		} 		
		return result;
	}
	private static String calculate() {
		String result = "";
		Model 	model = Model.getModel();
		
		String 	txt = model.getDisplay();
		String	mem = model.getMemory();
		Operation o = model.getPrevious();
		
		long lper1=0L;
		long lper2=0L;
		
		double dper1=0.0;
		double dper2=0.0;
		
		boolean isLong = false;
		
		if(txt.isEmpty()||mem.isEmpty()||o==null) {
			
		} else {
			
			isLong = txt.indexOf(".")==-1;
			if (isLong) {
				
				if (txt.indexOf(".")==mem.indexOf(".")) {
					lper1 = Long.parseLong(mem);
					lper2 = Long.parseLong(txt);
					
				} else {
					dper1 = Double.parseDouble(mem);
					dper2 = Double.parseDouble(txt);
					isLong = false;
				}
				
			} else {
				dper1 = Double.parseDouble(mem);
				dper2 = Double.parseDouble(txt);
			}
			
		}
		switch(o) {
		case ADD: 
			if (isLong) {
				long lper = lper1 + lper2;
				result = "" + lper;
			} else {
				double dper = dper1 + dper2;
				result = "" + dper;
			}
			break;
		case SUBTRACT: 
			if (isLong) {
				long lper = lper1 - lper2;
				result = "" + lper;
			} else {
				double dper = dper1 - dper2;
				result = "" + dper;
			}
			break;
		case MULTIPLY: 
			if (isLong) {
				long lper = lper1 * lper2;
				result = "" + lper;
			} else {
				double dper = dper1 * dper2;
				result = "" + dper;
			}
			break;
		case DIVIDE: 
			if (isLong) {
				long lper = lper1 / lper2;
				result = "" + lper;
			} else {
				double dper = dper1 / dper2;
				result = "" + dper;
			}
			break;
		
		}
		
		
		return result;
	}
	
}
