package calculator_mvc;

public class Model {
	
	static Model model;

	/*
	public class Value{
		public double	dValue;
		public long		lValue;
		
		public boolean 	isLong;
	}
	*/
	
	public enum Operation {
		DIGIT,
		CLEAR,
		DIVIDE,
		MULTIPLY,
		SUBTRACT,
		ADD,
		EQUAL,
		DOT,
		TEXT
	}
	

	String		memory = "";
	String 		display = "";
	int			lastInput;

	int			step; //0-input 1st;  1-input opetation; 2-input 2nd 
	Operation   previous;
	Operation   operation;
	
	private Model() {
		
	}
	
	public static Model getModel() {
		if (model==null) {
			model = new Model();
		}
		return model;
	}
	
	public static void main(String[] args) {	
	}


	public String getMemory() {
		return memory;
	}




	public void setMemory(String memory) {
		this.memory = memory;
	}




	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}


	
	public int getStep() {
		return step;
	}




	public void setStep(int step) {
		this.step = step;
	}




	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	
	public Operation getPrevious() {
		return previous;
	}

	public void setPrevious(Operation previous) {
		this.previous = previous;
	}

	public int getLastInput() {
		return lastInput;
	}

	public void setLastInput(int lastInput) {
		this.lastInput = lastInput;
	}

}
