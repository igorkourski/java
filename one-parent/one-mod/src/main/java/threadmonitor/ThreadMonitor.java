package threadmonitor;
import java.util.ArrayList;
import java.util.List;

public class ThreadMonitor {
	
	private List<ThreadInfo> 	completed;
	private List<ThreadInfo>	active;
	private List<ThreadInfo> 	delayed;
	
	static ThreadMonitor monitor;
	
	private ThreadMonitor() {
		completed = new ArrayList<ThreadInfo>();
		delayed = new ArrayList<ThreadInfo>();
		active = new ArrayList<ThreadInfo>();
	}
	
	public static synchronized ThreadMonitor get() {
		if (monitor==null) {
			monitor = new ThreadMonitor();
		}
		return monitor;
	}
	
	
	public synchronized void active(ThreadInfo thread) {
		active.add(thread);
	}
	private boolean removeActive(ThreadInfo thread) {
		return active.remove(thread);
	}
	
	
	public synchronized void completed(ThreadInfo thread) {
		thread.setDuration();
		
		completed.add(thread);
		removeActive(thread);
	}
	public synchronized void delayed(ThreadInfo thread) {
		thread.setDuration();
		
		delayed.add(thread);
		removeActive(thread);
	}
	
	private void report(List<ThreadInfo> list, String title) {
			
		System.out.println("\n" + title);
		
		if (list.isEmpty()) {
			
		} else {
			int i = 0;
			for (ThreadInfo thread : list) {
				System.out.println( i++ + ". " + thread.getName() + "  " + thread.getCreated() + "  "+ thread.getDuration() );
			}
		}
	}

	
	public synchronized void report() {
		
		report(completed,"completed");
		report(delayed,"delayed");
		report(active,"active");

	}

}
