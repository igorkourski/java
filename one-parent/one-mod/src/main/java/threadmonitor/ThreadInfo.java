package threadmonitor;
import java.util.Date;

public class ThreadInfo {
	
	private Date	created;
	private long    started;
	private long 	duration;
	
	private long 	id;
	private String	name;
	
	private int		status;
	private String	info;
	private int		step;
	
	public ThreadInfo(Thread thread) {
		created = new Date();
		started = System.nanoTime();
		
		this.name = thread.getName();
		this.id = thread.getId();
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration() {
		this.duration = System.nanoTime() - started;
	}
	
	public void setDuration(long duration) {
		this.duration = duration;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	public void setStep(int	step) {
		this.step = step;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStep() {
		return step;
	}
	
}
