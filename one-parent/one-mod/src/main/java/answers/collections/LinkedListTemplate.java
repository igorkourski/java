package collections;

public class LinkedList<T> {
	
	//Node impl
	public class Node<T> implements Comparable<T> {
		 
		private T 		nodeValue;
		private Node<T> nodeNextReference;
	 
		public T getValue() {
			return nodeValue;
		}
	 
		public void setValue(T nodeValue) {
			this.nodeValue = nodeValue;
		}
	 
		public Node<T> getNextReference() {
			return nodeNextReference;
		}
	 
		public void setNextRef(Node<T> nodeNextReference) {
			this.nodeNextReference = nodeNextReference;
		}
	 
		// @Override indicates that a method declaration is intended to override a method declaration in a supertype.
		public int compareTo(T nodeValue) {
			if (nodeValue == this.nodeValue) {
				return 0;
			} else {
				return 1;
			}
		}
	}
	// end of Node impl

	private Node<T> headElement;
	 
	// Iterate through elements in Node
	public String toString() {
		String res = "";
		Node<T> tempVariable = headElement;
		while (true) {
			if (tempVariable == null) {
				break;
			}
			res+=tempVariable.getValue() + "  ";
			tempVariable = tempVariable.getNextReference();
		}
		return res;
	}
 
	// Reverse elements in Node
	public void reverse() {
 
		Node<T> previousElement = null;
		Node<T> currentElement = headElement;
		Node<T> nextElement = null;
		while (currentElement != null) {
			nextElement = currentElement.getNextReference();
			currentElement.setNextRef(previousElement);
			previousElement = currentElement;
			currentElement = nextElement;
		}
		headElement = previousElement;
	}
 
	// Add elements to Node
	public void addElement(T element) {
 
		Node<T> Node = new Node<T>();
		Node.setValue(element);

		Node<T> tempVariable = headElement;
		while (true) {
			if (tempVariable == null) {																															
				headElement = Node; // Only 1 element? Head and Tail points to the same
				break;
			} else 
				if (tempVariable.getNextReference() == null) {
					tempVariable.setNextRef(Node);
					break;
				} else {
					tempVariable = tempVariable.getNextReference();
				}
		}
	}
	
}
