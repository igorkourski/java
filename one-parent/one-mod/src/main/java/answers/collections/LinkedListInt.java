package collections;

public class LinkedListInt {
	
	private class Node{
		private Node	next;
		private int 	data;
		
		private Node(int data) {
			this.data = data;
		}
		
		public Node getNext() {
			return next;
		}
		public void setNext(Node next) {
			this.next = next;
		}
		public int getData() {
			return data;
		}
		//public void setData(int data) {
		//	this.data = data;
		//}
	}

	private Node		head;
	private int			counter;
	
	public LinkedListInt() {
		head = null;
		counter = 0;
	}

	//add to the end of list
	public void add(int object) {
		if (head==null) {
			head = new Node(object);
		}
		
		Node temp = new Node(object);
		Node current =  head;
		
		if(current!=null) {
			
			while(current.getNext()!=null) {
				current = current.getNext();
			}
			current.setNext(temp);
			
		}
		counter++;
		
	}
	
	//-1 error
	public int get(int index) {
		
		// index must be 1 or higher
		if (index < 0)
			return -1;
		
		Node current = null;
		if (head != null) {
			current = head.getNext();
			for (int i = 0; i < index; i++) {
				if (current.getNext() == null)
					return -1;
 
				current = current.getNext();
			}
			return current.getData();
		}
		return -1;
	}
	
	public boolean remove(int index) {
		// if the index is out of range, exit
		if (index < 1 || index > size())
			return false;
 
		Node current = head;
		if (head != null) {
			for (int i = 0; i < index; i++) {
				if (current.getNext() == null)
					return false;
 
				current = current.getNext();
			}
			current.setNext(current.getNext().getNext());
 
			// decrement the number of elements variable
			counter--;
			return true;
 
		}
		return false;
		
	}
	
	public String toString() {
		String output = "";
 
		if (head != null) {
			Node current = head.getNext();
			while (current != null) {
				output += "[" + current.getData() + "]";
				current = current.getNext();
			}
 
		}
		return output;
	}
	
	public int size() {
		return counter;
	}
}
