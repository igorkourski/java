package threadpool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import threadmonitor.ThreadMonitor;

public class TaskPool {
	
	private int 		execSize = 5;
	private int 		poolSize = 10;
	private Class   	execClass = null;
	//private List<TaskExec> pool = new ArrayList<TaskExec>();
	
	public void run() {
		
        // creates a thread pool with MAX_POOL no. of  
        // threads as the fixed pool size(Step 1) 
        
		ExecutorService pool = Executors.newFixedThreadPool(execSize);   
        
        List<Future<?>> futures = new ArrayList<Future<?>>();
    	
    	// creates tasks 
    	for (int i=0; i<poolSize; i++) {
   	       	//create tast (Step 2)
    		Thread thread = createThread("task " + i);
    		
    		Runnable r = thread;
   	       
    		// passes the Task objects to the pool to execute (Step 3) 
    		Future f = pool.submit(r);
    		futures.add(f);
    		
    	}
		
	    //List<Future<?>> futures = init();
		 
        
    	//instead of shutdown (i.e. not accepting new tasks
        //pool.shutdown(); 
    	//Use A) or B)
    	// A) Await all runnables to be done (blocking)
    	//for(Future<?> future : futures)
    	//    future.get(); // get will block until the future is done

    	// B) Check if all runnables are done (non-blocking)
    	//boolean allDone = true;
    	//for(Future<?> future : futures){
    	//    allDone &= future.isDone(); // check if future is done
    	//}
    	
       	//A) Await all runnables to be done (blocking)
    	for(Future<?> future : futures)
			try {
				future.get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // get will block until the future is done

    	
    	ThreadMonitor.get().report();
		
	}
	
	public List<Future<?>> init() {
/*		
		pool = Executors.newFixedThreadPool(execSize);  
				
        List<Future<?>> futures = new ArrayList<Future<?>>();
    	
    	// creates tasks 
    	for (int i=0; i<poolSize; i++) {
   	       	//create tast (Step 2)
    		Runnable r = new TaskExec("task " + i); 
   	       
    		// passes the Task objects to the pool to execute (Step 3) 
    		Future f = pool.submit(r);
    		futures.add(f);
    		
    	}	
    	*/
		return null;
	}
	
	public void setExecSize(int size) {
		execSize = size;
	}
	
	public void setPoolSize(int size) {
		poolSize = size;
	}
	
	public void setExec(Class exec) {
		this.execClass = exec;
	}

	@SuppressWarnings("deprecation")
	private Thread createThread(String title) {
		
		TaskExec exec = null;
		try {
			exec = (TaskExec)execClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (exec!=null) {
			exec.setTitle(title);
		}
		
		return exec;
		
	}
}
